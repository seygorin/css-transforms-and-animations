# CSS-Transforms-and-Animations

This markup will come in handy at the end of the HTML and CSS course as you will create a full landing page based on this markup. <br/>
Visual presentation on [Figma Project](https://www.figma.com/file/qIyiRXgAFeHqerGu7eS1Ez/04-CSS-Selectors-and-Responsive?node-id=0%3A1).

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Task

The things you need before installing the software.

* Install any IDE or use the one that is already installed (for example, Visual Studio Code)
* Get your account in a public GitLab repository and create a project named "CSS-Transforms-and-***"
* Create a local folder for the future project or clone your earlier project "Prev-Task", or copy files from it

### Installation

A step by step guide that will tell you how to get the development environment up and running.

```
git clone https://gitlab.com/seygorin/css-transforms-and-animations.git
```
